import React from "react";
import styled from "styled-components";
import { Link, useHistory } from "react-router-dom";
import Button from "../Button/Button";

const CardStyled = styled.div`
  width: 500px;
  padding: 20px;
  border: 1px solid red;

  &:hover {
    background-color: green;
  }
`;

const StyledParagraph = styled.p`
  color: red;
`;

const renderPost = () => "hello";

const Card = ({ id, title, description, onDelete }) => {
  const history = useHistory();
  return (
    <CardStyled>
      <div onClick={() => onDelete(id)}>
        <h1>{title}</h1>
        <StyledParagraph as="h3">{description}</StyledParagraph>
      </div>
      {/* <Link to={`/post/${id}`}>Read more</Link> */}
      <Button
        onClick={() =>
          history.push(`/post/${id}`, { data: { title, description } })
        }
        color="blue"
      >
        Read more
      </Button>
    </CardStyled>
  );
};

export { renderPost };
export default Card;
