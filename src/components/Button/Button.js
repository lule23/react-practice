import styled from "styled-components";

const Button = styled.button`
  background-color: ${(props) => props.color};
  color: white;
`;

export default Button;
