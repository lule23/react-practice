import React, { useState, useEffect } from "react";
import axios from "axios";

const PostPage = ({ history, location, match }) => {
  const postID = match.params.ID;
  // const [post, setPost] = useState(null);

  console.log(location);

  const post = location.state.data;

  // useEffect(() => {
  //   const fetchPost = async () => {
  //     try {
  //       const response = await axios.get(
  //         `https://jsonplaceholder.typicode.com/posts/${postID}`
  //       );

  //       setPost(response.data);
  //     } catch (error) {
  //       console.log(error);
  //     }
  //   };

  //   fetchPost();
  // }, []);

  if (!post) {
    return <div>Loading...</div>;
  }

  return (
    <div>
      <h1>{post.title}</h1>
      <p>{post.description}</p>
    </div>
  );
};

export default PostPage;
