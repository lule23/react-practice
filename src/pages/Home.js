import React, { useState, useEffect } from "react";
import axios from "axios";
import Card from "../components/Card";
import Button from "../components/Button/Button";

const HomePage = () => {
  const [state, setState] = useState({
    posts: [],
    users: [],
    fetchUsers: false,
  });

  useEffect(() => {
    const fetchPosts = async () => {
      try {
        const response = await axios.get(
          "https://jsonplaceholder.typicode.com/posts"
        );
        setState((prevState) => ({ ...prevState, posts: response.data }));
      } catch (error) {}
    };

    fetchPosts();

    const timeout1 = setTimeout(() => alert("hello"), 2500);

    return () => {
      //  ciscenje nepotrebnih stvari iz ove komponente
      clearTimeout(timeout1);
    };
  }, []);

  useEffect(() => {
    const fetchUsers = async () => {
      try {
        const response = await axios.get(
          "https://jsonplaceholder.typicode.com/users"
        );
        setState((prevState) => ({ ...prevState, users: response.data }));
      } catch (error) {}
    };

    state.fetchUsers && fetchUsers();
  }, [state.fetchUsers]);

  const deletePost = (id) => {
    setState((prevState) => ({
      ...prevState,
      posts: [...prevState.posts].filter((post) => post.id !== id),
    }));
  };

  return (
    <div className="App">
      {state.posts.map((post) => {
        return (
          <Card
            key={post.id}
            id={post.id}
            title={post.title}
            description={post.body}
            onDelete={deletePost}
          />
        );
      })}
      <Button
        color="red"
        onClick={() =>
          setState((prevState) => ({ ...prevState, fetchUsers: true }))
        }
      >
        Fetch users
      </Button>
    </div>
  );
};

export default HomePage;
