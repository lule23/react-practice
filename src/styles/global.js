import { createGlobalStyle } from "styled-components";

const GlobalStyle = createGlobalStyle`
    *,
    *::after,
    *::before {
        box-sizing: border-box;
    }

    html {
        box-sizing: inherit;
    }


    body {
        margin: 0;
        padding: 0;
        font-family: ${({ theme }) => theme.fonts.primary};
        color: ${({ theme }) => theme.colors.primary};
    }
`;

export default GlobalStyle;
