const lightTheme = {
  colors: {
    primary: "#333",
    secondary: "green",
  },
  fonts: {
    primary: "'Roboto'",
    secondary: "",
  },
};

const darkTheme = {
  colors: {
    primary: "blue",
    secondary: "green",
  },
  fonts: {
    primary: "'Roboto'",
    secondary: "",
  },
};

const theme = (type) => {
  return type === "light" ? lightTheme : darkTheme;
};

export default theme;
