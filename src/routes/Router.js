import React from "react";
import { BrowserRouter, Switch, Route, Redirect } from "react-router-dom";
import HomePage from "../pages/Home";
import PostPage from "../pages/Post";

const Router = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/post/:ID" component={PostPage} />
        <Route path="/" component={HomePage} exact />
        <Route component={<Redirect to="/" />} />
      </Switch>
    </BrowserRouter>
  );
};

export default Router;
