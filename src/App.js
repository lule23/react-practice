import React from "react";
import { ThemeProvider } from "styled-components";
import theme from "./styles/theme";
import GlobalStyle from "./styles/global";
import Router from "./routes/Router";

function App() {
  return (
    <React.Fragment>
      <ThemeProvider theme={() => theme("light")}>
        <Router />
        <GlobalStyle />
      </ThemeProvider>
    </React.Fragment>
  );
}

export default App;
